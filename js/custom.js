// Preloader 
jQuery(function (jQuery) {
    //Preloader
    var preloader = jQuery('.preloader');
    jQuery(window).load(function () {
        preloader.remove();
    });
});
// Wow 
wow = new WOW({
    boxClass: 'wow', // default
    animateClass: 'animated', // default
    offset: 0, // default
    mobile: false, // default
    live: true // default
})
wow.init();
// Bootstrap Slider 
jQuery('.carousel').carousel({
    interval: 20000
});
/**** owl carousel ****/
jQuery(document).ready(function () {
    var owl = jQuery("#logo-demo");
    owl.owlCarousel({
        itemsCustom: [
                [767, 2]
                , [992, 3]
                , [1200, 4]
                , [1500, 4]]
        , navigation: true
        , pagination: true
        , slideSpeed: 1000
        , scrollPerPage: true
        , autoPlay :true
    });
});
jQuery(document).ready(function () {
    var owl = jQuery("#testimonial-demo");
    owl.owlCarousel({
        itemsCustom: [
            [200, 1]
                , [767, 1]
                , [992, 1]
                , [1200, 1]
                , [1500, 1]]
        , navigation: true
        , pagination: true
        , slideSpeed: 1000
        , scrollPerPage: true
         , autoPlay :true
    });
    
    
});
/**** Textarea First Letter Capital ****/
jQuery('textarea.form_control1').on('keypress', function (event) {
    var $this = jQuery(this)
        , thisVal = $this.val()
        , FLC = thisVal.slice(0, 1).toUpperCase();
    con = thisVal.slice(1, thisVal.length);
    jQuery(this).val(FLC + con);
});
/**** Filter ****/
     jQuery(function () {
            var filterList = {
                init: function () {
                    // MixItUp plugin
                    // http://mixitup.io
                    jQuery('#portfoliolist').mixItUp({
                        selectors: {
                            target: '.portfolio'
                            , filter: '.filter'
                        }
                        , load: {
                            filter: '.multi_page'
                        }
                    });
                }
            };
            // Run the show!
            filterList.init();
            	jQuery(' #da-thumbs figure ').each( function() { jQuery(this).hoverdir(); } );
     
        });


